package ru.potapov.tm.command;

public enum SortBy {
    ByCreate,
    ByDateStart,
    ByDateFinish,
    ByStatus;
}
