package ru.potapov.tm.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.entity.User;

import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public final class UserReadAllCommand extends AbstractCommand {
    public UserReadAllCommand(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
    }

    @NotNull
    @Override
    public String getName() {
        return "user-read-all";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Lists all users";
    }

    @Override
    public void execute(@Nullable final String ... additionalCommands) throws Exception {
        if (!super.allowedRun())
            return;
        if (Objects.isNull(getServiceLocator()))
            return;

        getServiceLocator().getTerminalService().printlnArbitraryMassage("All users:");
        for (@NotNull final User user : getServiceLocator().getUserService().getCollectionUser()) {
            getServiceLocator().getTerminalService().printlnArbitraryMassage(user.getLogin() + " [" + user.getRoleType() + "]");
        }
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
}
