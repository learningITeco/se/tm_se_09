package ru.potapov.tm.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;

import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public final class HelpCommand extends AbstractCommand {
    public HelpCommand(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
    }

    @NotNull
    @Override
    public String getName() {
        setNeedAuthorize(false);
        return "help";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Lists all command";
    }

    @Override
    public void execute(@Nullable final String ... additionalCommands) throws Exception {
        if (!super.allowedRun())
            return;
        if (Objects.isNull(getServiceLocator()))
            return;

        for (final AbstractCommand command : getServiceLocator().getTerminalService().getListCommands()) {
            getServiceLocator().getTerminalService().printlnArbitraryMassage(command.getName() + ": " + command.getDescription());
        }
    }
}
