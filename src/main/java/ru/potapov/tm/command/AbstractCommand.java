package ru.potapov.tm.command;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.api.ServiceLocator;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractCommand {
    private boolean needAuthorize           = true;
    @Nullable protected Bootstrap bootstrap;
    @Nullable private ServiceLocator serviceLocator = bootstrap;

    public AbstractCommand(@Nullable final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    public abstract void execute(@Nullable final String ... additionalCommands) throws Exception;

    public boolean allowedRun(){
        if (Objects.isNull(getServiceLocator()) || Objects.isNull(getServiceLocator().getUserService()))
            return false;

        if ( needAuthorize && !getServiceLocator().getUserService().isAuthorized() ){
            getServiceLocator().getTerminalService().printMassageNotAuthorized();
            return false;
        }
        return true;
    }

    public void setBootstrap(@Nullable final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }
}
