package ru.potapov.tm.api;

import org.jetbrains.annotations.Nullable;

public interface Entity {
    @Nullable String getId();
    @Nullable String getName();
    @Nullable String getUserId();
}
