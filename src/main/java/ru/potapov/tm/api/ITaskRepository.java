package ru.potapov.tm.api;


import org.jetbrains.annotations.NotNull;

import java.util.Collection;

public interface ITaskRepository<T extends Entity> extends IRepository<T> {
    @NotNull Collection findAll(@NotNull final String userId,@NotNull final  String idProject);
    @NotNull Collection findAllByUser(@NotNull final String userI);
}
