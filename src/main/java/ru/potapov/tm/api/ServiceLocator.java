package ru.potapov.tm.api;

import org.jetbrains.annotations.NotNull;

public interface ServiceLocator {
    @NotNull IProjectService getProjectService();
    @NotNull ITaskService getTaskService();
    @NotNull ITerminalService getTerminalService();
    @NotNull IUserService getUserService();
}
