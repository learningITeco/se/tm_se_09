package ru.potapov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface IRepository<T extends Entity> {
    @NotNull Collection<T> findAll();
    @NotNull Collection<T> findAll(@NotNull final String userId);
    @Nullable T findOne(@NotNull final String name);
    @Nullable T findOne(@NotNull final String userId, @NotNull final String name);
    @Nullable T findOneById(@NotNull final String id);
    @Nullable T findOneById(@NotNull final String userId, @NotNull final String id);
    void persist(T t);
    void persist(@NotNull final String userId, T t);
    @NotNull T merge(@NotNull final T tNew);
    @NotNull T merge(@NotNull final String userId, T tNew);
    void remove(@NotNull final T t);
    void remove(@NotNull final String userId, @NotNull final T t);
    void removeAll();
    void removeAll(@NotNull final String userId);
    void removeAll(@NotNull final Collection<T> list);
    void removeAll(@NotNull final String userId, @NotNull final Collection<T> list);
    @NotNull Collection<T> getCollection();
    @NotNull Collection<T> getCollection(@NotNull final String userId);
}
