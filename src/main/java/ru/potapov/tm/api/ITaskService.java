package ru.potapov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.api.ITaskRepository;

import java.time.format.DateTimeFormatter;
import java.util.Collection;

public interface ITaskService {
    @NotNull ITaskRepository<Task> getTaskRepository();
    int checkSize();
    @Nullable Task findTaskByName(@NotNull final String name);
    void remove(@NotNull final Task task);
    void removeAll(@NotNull final String userId);
    void removeAll(@NotNull final Collection<Task> listTasks);
    @NotNull Task renameTask(@NotNull final Task task, @NotNull final String name) throws CloneNotSupportedException;
    void changeProject(@NotNull final Task task, @NotNull final Project project) throws CloneNotSupportedException;
    @NotNull Collection<Task> findAll(@NotNull final String idProject);
    @NotNull Collection<Task> findAll(@NotNull final String userId, @NotNull final String idProject);
    @NotNull Collection<Task> findAllByUser(@NotNull final String userId);
    void put(Task task);
    @NotNull String collectTaskInfo(@NotNull final Task task, @NotNull final DateTimeFormatter ft);
}
