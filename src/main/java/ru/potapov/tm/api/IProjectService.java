package ru.potapov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.Project;


import java.time.format.DateTimeFormatter;
import java.util.Collection;

public interface IProjectService {
    @Nullable IProjectRepository getProjectRepository();
    int checkSize();
    @Nullable Project findProjectByName(@NotNull final String name);
    @Nullable Project findProjectByName(@NotNull final String userId, @NotNull final String name);
    @NotNull Collection<Project> getCollection(@NotNull final String userId);
    @NotNull Project renameProject(@NotNull final Project project, @NotNull final String name) throws CloneNotSupportedException;
    void removeAll(@NotNull final String userId);
    void removeAll(Collection<Project> listProjects);
    void remove(@NotNull final Project project);
    void put(@NotNull final Project project);
    @NotNull String collectProjectInfo(@NotNull final Project project, @NotNull final String owener, @NotNull final DateTimeFormatter ft);
}
